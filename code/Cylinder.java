//Jacky Tat
public class Cylinder {
    private double height;
    private double radius;

    public Cylinder(double height, double radius){
        this.height = height;
        this.radius = radius;
    }

    public double getHeight(){
        return this.height;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getVolume(){
        return Math.PI*Math.pow(this.radius, 2)*this.height;
    }

    public double getSurfaceArea(){
        return (2*Math.PI*Math.pow(this.radius, 2))+(2*Math.PI*this.radius*this.height);
    }
}
