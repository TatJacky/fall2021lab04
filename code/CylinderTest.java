//Xuanru Yang, 1942014
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class CylinderTest {
    @Test
    public void getHeightTest() {
        Cylinder cylinder = new Cylinder(3, 4);
        assertEquals(3, cylinder.getHeight());
    }

    @Test
    public void getRadiusTest() {
        Cylinder cylinder = new Cylinder(5, 8);
        assertEquals(8, cylinder.getRadius());
    }

    @Test
    public void getVolumeTest() {
        Cylinder cylinder = new Cylinder(5, 7);
        assertEquals(Math.PI*Math.pow(7, 2)*5, cylinder.getVolume());
    }

    @Test
    public void getSurfaceAreaTest() {
        Cylinder cylinder = new Cylinder(3, 6);
        assertEquals(2*Math.PI*Math.pow(6, 2)+2*Math.PI*6*3, cylinder.getSurfaceArea());
    }
}
