//Jacky Tat 1911748
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Spheretests{
    @Test
    public void testConstructor(){
        Sphere s = new Sphere(3);
        assertEquals(3, s.getRadius());
    }

    @Test
    public void testGetVolume(){
        Sphere s = new Sphere(3);
        assertEquals(Math.PI*Math.pow(3, 3), s.getVolume());
    }

    @Test
    public void testGetSurfaceArea(){
        Sphere s = new Sphere(3);
        assertEquals(4*Math.PI*Math.pow(3, 2), s.getSurfaceArea());
    }
}
