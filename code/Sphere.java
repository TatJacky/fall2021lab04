//Xuanru Yang, 1942014
public class Sphere {
    public static void main(String[] args) {
        
    }

    private double radius;

    public Sphere(double radius) {
        this.radius = radius;
    }
    
    public double getRadius() {
        return radius;
    }

    public double getVolume() {
        return Math.PI*Math.pow(radius, 3);
    }

    public double getSurfaceArea() {
        return 4*Math.PI*Math.pow(radius, 2);
    }
}